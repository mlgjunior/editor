(function () {

  //Config MathJax to load on page
  MathJax.Hub.Config({
    extensions: ["tex2jax.js"],
    TeX: {equationNumbers: {autoNumber: "AMS"}},
    jax: ["input/TeX","output/HTML-CSS"],
    tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]]}
  });

  //Shorthand for the queue
  var QUEUE = MathJax.Hub.queue;  

  //The element jax for the math output, and the box it's in
  var math = null, box = null;    

  //Hide and show the box (so it doesn't flicker as much)
  var HIDEBOX = function () {box.style.visibility = "hidden"}
  var SHOWBOX = function () {box.style.visibility = "visible"}

  //Get the element jax when MathJax has produced it.
  QUEUE.Push(function () {
    math = MathJax.Hub.getAllJax("MathOutput")[0];
    box = document.getElementById("box");
    SHOWBOX(); // box is initially hidden so the braces don't show
  });

  /**
   * UpdateMath
   *
   * The onchange event handler that typesets the math entered
   * by the user.  Hide the box, then typeset, then show it again
   * so we don't see a flash as the math is cleared and replaced.
   * 
   * @param String TeX
   */
  window.UpdateMath = function (TeX) {
    console.log(math);
    QUEUE.Push(HIDEBOX,["Text",math,"\\displaystyle{"+TeX+"}"],SHOWBOX);
  }

  })();

  /**
   *  Force onkeypress IE
   *  
   *  IE doesn't fire onchange events for RETURN, so
   *  use onkeypress to do a blur (and refocus) to
   *  force the onchange to occur
   */
  if (MathJax.Hub.Browser.isMSIE) {
    MathInput.onkeypress = function () {
      if (window.event && window.event.keyCode === 13) {this.blur(); this.focus()}
    }
  }

  /**
   * setFormula
   *
   * @author  Maurício Junior
   * @since  03/2015
   * @param Object button_formula 
   */
  function setFormula( button_formula )
  {
    //Iniatialize formula and get the current valor if exist
    var formula = button_formula.value;
    var mathInput = $("#MathInput");
    var current_valor = mathInput.val();

    //Set current valor 
    if(current_valor.length > 0) formula = current_valor + formula;
    
    //Set formula on input value and update the SVG
    mathInput.val(formula);
    UpdateMath(mathInput.val());   
  }

  /**
   * [changeValues description]
   * @param  object obj      [description]
   * @param  {[type]} constant [description]
   * @return {[type]}          [description]
   */
  function changeValues( obj, constant)
  {
    var value = obj.value;
    var mathInput = $("#MathInput");

    var result = mathInput.val().replace(constant, value);
    
    mathInput.val(result);
    UpdateMath(result);

  }

  // function changeX( obj )
  // {
  //   var x_value = obj.value;
  //   var mathInput = $("#MathInput");

  //   var res = mathInput.val().replace('x', x_value);
  //   mathInput.val(res);

  //   UpdateMath(res);
  // }

  // function changeY( obj )
  // {
  //   var x_value = obj.value;
  //   var mathInput = $("#MathInput");

  //   var res = mathInput.val().replace('y', x_value);
  //   mathInput.val(res);

  //   UpdateMath(res);
  // }

